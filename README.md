Esta es una guía de como instalar y configurar un servidor simple para hostear 
una página web estática con _nginx_, para manejar todo remotamente con _ssh_, 
para sincronizar documentos (o código fuente) con _git_ y _ssh_, y para
compartir archivos con _nfs_.

---------------

La guía está escrita en _Markdown_ (_md_), y después genero un _HTML_ con [_Pandoc_](http://pandoc.org/]):

~~~
pandoc servidor_debian.md -o servidor_debian.html -c servidor_debian.css --toc --toc-depth 2
~~~

El _CSS_ es ```servidor_debian.css```.

También puede ser útil tener la guía en formato _PDF_, así que la generé con:

~~~
pandoc servidor_debian.md -o servidor_debian.pdf
~~~

---------------

Para leer la guía en _HTML_: <https://mbernardi.gitlab.io/guia_servidor_debian/>

Para leer la guía en _PDF_: <https://mbernardi.gitlab.io/guia_servidor_debian/servidor_debian.pdf>
