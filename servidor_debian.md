% Configuración de un servidor simple en Debian

<!---
pandoc servidor_debian.md -o servidor_debian.html -c servidor_debian.css --toc --toc-depth 2

pandoc servidor_debian.md -o servidor_debian.pdf
--->

Esta es una guía de como instalar y configurar un servidor simple para hostear 
una página web estática con _nginx_, para manejar todo remotamente con _ssh_, 
para sincronizar documentos (o código fuente) con _git_ y _ssh_, y para
compartir archivos con _nfs_.

Como extra también está la configuración de un servidor de impresión _CUPS_.

No voy a usar ningún entorno gráfico así que hay que saber usar un poco la
consola, igual voy a dar todos los comandos, así que no hace falta mucha
experiencia. Hace falta saber crear carpetas (```mkdir```), copiar y mover
(```cp``` y ```mv```) y saber usar algún editor de texto para la consola
(```nano```, ```vim``` o ```emacs```).

También hay que saber un poco de cómo funciona una red (IP local, IP pública,
gateway, máscara de red, etc.).

El servidor puede ser una PC vieja, con 1GB de RAM (o menos), un procesador
viejo, lo que sea. Depende de el uso, si se quieren ver videos por red es
importante que el disco, el procesador y la red sean lo suficientemente rápidos.
Si hay que comprar algo estaría bueno armar una red Gigabit (hasta 1Gb/s)
(_gigabit_, no _gigabyte_).

Yo diría que primero hay que armar una PC con lo que se encuentre y después si
hace falta se compra algo. Al principio se necesita monitor y teclado (mouse
no), pero una vez configurado _SSH_, del servidor solamente van a salir dos
cables: el de energía y el de red.

**Por qué elegí esos programas**

_nginx_
:    Es un servidor web muy usado y liviano. Antes había usado _Apache_ que
     sería la alternativa pero leí que es más pesado y además quería probar algo
     distinto. Igual son muy parecidos (para configurar).

_ssh_
:    Es un protocolo para acceder a una PC remotamente, hay alternativas pero
     es lejos la más usada, además es muy seguro.

_git_
:    En realidad es un software de control de versiones pensado para código
     fuente (para programar), pero me gusta usarlo para sincronizar documentos
     porque me parece confiable y porque mantiene todas las versiones antiguas
     de todos los archivos. Además lo más importante puede ser que mantiene una
     copia de **todo** en **todos los equipos** para trabajar sin red y como
     backup por si se rompe el servidor. Como desventaja puede ser poco molesto
     para sincronizar porque es muy técnico.

_nfs_
:    Es lo más común para compartir alguna carpeta en los equipos de la red.
     Es más común para equipos _GNU/Linux_, pero _Windows_ también lo soporta.
     Como alternativa si hay muchos equipos con _Windows_ se puede usar
     _samba_, pero creo que es más difícil de configurar.

_cups_
:    Es lo que se usa para la impresión en _GNU/Linux_, está bueno crear un
     servidor de _CUPS_ porque es posible conectar todas las impresoras al
     servidor y imprimir desde cualquier PC en la red local.
     La mejor ventaja es que en la mayoría de los casos la impresora funciona en
     el primer intento y no hay que renegar descargando y probando drivers.
     En mi caso que tengo _HP_ los drivers son siempre una porquería
     independientemente de la plataforma, pero sorprendentemente es más fácil
     hacer funcionar todo desde _GNU/Linux_, aunque con la desventaja de que las
     impresiones son más lentas y faltan funcionalidades.



El sistema operativo
====================

Me gusta _Debian_ y además es perfecto para servidores en la versión _Stable_.

En mi caso usé _Debian Stable 8.40 Jessie CD1_.

**Qué significa:**

Debian
:    El sistema operativo.

Stable
:    La "rama" utilizada, Debian tiene dos ramas principales para elegir,
     _Stable_ que sigifica "estable", no es de actualizarse mucho, y _Testing_ 
     que significa "en pruebas", no es que sea muy inestable, es lo recomendado 
     para el usuario común porque tiene cosas más nuevas.

8.4 Jessie
:    Es la versión que en este momento es _Stable_. En un tiempo, la versión que
     actualmente es _Testing_ (_Squeeze_) pasará a ser _Stable_ y aparecerá una
     nueva versión como _Testing_.

CD1
:    Hay varios medios de instalación para elegir, los más comunes son
     _netinstall_ y los CDs. _netinstall_ necesita internet pero los CDs no.
     Hay muchos CDs, porque no entran todos los programas en uno solo, en el
     primer CD están los programas básicos, a los demás los descargo por
     internet

Creo que lo recomendable es _Debian Stable_, usando cualquier medio de
instalación. 

Soy de usar el _CD1_ porque tiene lo necesario para una instalación
básica sin internet, que es importante porque _Debian_ no incluye software
no libre en sus medios de instalación, y muchos equipos necesitan firmware no
libre como en mi caso para acceder a internet. Cuando se termina la instalación
uno puede instalar el firmware desde un USB, y una vez que se puede entrar a
internet uno puede descargar más software.

Como alternativa hay una imagen de instalación no oficial que incluye software
no libre, o también uno puede instalar firmware no libre desde un USB justo
antes de comenzar a instalar (por ejemplo en el caso de un _netinstall_) pero
nunca pude hacerlo funcionar.

Instalación
===========

Hay muchas guías paso a paso de instalación de _Debian_. Soy de instalar
con la opción _Install_ normal, no uso _Graphical Install_. Son lo mismo, uno
es mas lindo que el otro nada más.

Es buena práctica no tener usuario _root_, en cambio se debería usar un usuario
común con permisos de _sudo_ que explico después. Al instalar hay que leer bien
lo que va diciendo el instalador, en un momento va a preguntar si se quiere
crear un usuario _root_ (si se instala en modo texto en un paso hay que poner
la contraseña de _root_ vacía para que el instalador cree un usuario con
permisos de _sudo_, hay que leer bien).

Se pueden instalar algunas carpetas en particiones por separado pero no es
muy necesario para un servidor simple. Yo instalé ```/home``` en una partición
por separado pero no es obligatorio. 

```/home``` es la carpeta en donde se guardan todos los archivos de los usuarios
(fotos, videos, documentos, etc.) y en donde voy a poner todas las cosas 
(menos el sitio web que por convención se guarda en ```/var/www```).

Tengo un disco de 500GiB y aparte de la partición _EFI_ del sistema tengo la
partición principal (llamada ```/```) de 40GiB, la partición de _swap_ de 8GiB,
y la partición ```/home``` con lo que quedó del disco.

Una vez que se llega a una selección de el software a instalar se debería
seleccionar sólo "Utilidades estándar del sistema", no se debería instalar
entorno de escritorio, lo demás se puede instalar a mano muy fácil.

![Selección de software](imagenes/debian_instalacion.png)

Configuración
=============

Notas
-----

Hay algunas cosas que hay que saber hacer en la consola:

- Instalar programas
- Manejarse con los usuarios
- Entender cómo se nombran las particiones y discos
- Montar un pendrive
- Entender los permisos
- Editar archivos de texto

### Instalar programas

En _Debian_ los programas vienen en _paquetes_ y se instalan con

~~~{.bash}
sudo apt-get install nombre_de_paquete
~~~

Normalmente el nombre del paquete es el mismo que el del programa, para borrar:

~~~{.bash}
sudo apt-get remove nombre_de_paquete
~~~

_apt-get_ es el nombre del programa que maneja los paquetes, a _sudo_ lo explico
después.

### Manejo de usuarios

Hay un usuario especial llamado _root_ que puede hacer lo que quiera, sería el
administrador o superusuario. No vamos a usar ese usuario por seguridad, en
cambio usamos un usuario común que tiene permisos de _sudo_, significa que
cada vez que hay que hacer algo que necesite permisos de superusuario hay que
anteponer la palabra ```sudo``` al comando.

Voy a poner _sudo_ a todos los comandos que lo necesiten.

Para cambiar el usuario hay que usar:

~~~{.bash}
su nombre_de_usuario
~~~

Y va a pedir la contraseña del usuario. Como truco, si el usuario actual tiene
permisos de _sudo_, si uno hace:

~~~{.bash}
sudo su nombre_de_usuario
~~~

Se ahorra de escribir la contraseña.

### Nomenclatura de discos y particiones

Los discos se nombran empezando con ```sd```, después viene una letra (van en
orden alfabético) que identifica el disco y después un número que identifica
la partición. Entonces por ejemplo ```sdc2``` es la segunda partición del
tercer disco.

Para saber que partición elegir se puede usar el comando ```lsblk```.

### Montaje (Para usar pendrives)

Para usar un pendrive (o un CD, DVD, etc. pero ya están viejos) se debe conectar
y después se lo debe **montar** en una carpeta. De esa forma todos los archivos
que estén dentro del pendrive van a aparecer en esa carpeta y después se puede
copiar, pegar y editar cosas normalmente. Al final antes de sacar el pendrive se
lo debe **desmontar**

Hay que acordarse de crear primero la carpeta en donde se va a montar,
normalmente uso ```/mnt/usb```

~~~{.bash}
sudo mkdir /mnt/usb
~~~

Para montar:

~~~{.bash}
sudo mount /dev/sd?? /mnt/usb
~~~

Se debe reemplazar ```sd??``` por la partición correcta. Se puede elegir
otra carpeta en lugar de ```/mnt/usb```

Para desmontar:

~~~{.bash}
sudo umount /dev/sd??
~~~

o que es lo mismo

~~~{.bash}
sudo mount /mnt/usb
~~~

### Permisos

Todos los archivos tienen un usuario y un grupo que son propietarios del
archivo, y además tienen una serie de permisos.

Los permisos son tres, de lectura, de escritura y de ejecución (para programas
ejecutables), y uno puede elegir cuáles de esos permisos están habilitados para
el usuario, para los usuarios del grupo, y para los demás usuarios. Normalmente
ignoro a los grupos porque no son tan necesarios.

Para elegir el dueño del archivo se usa:

~~~{.bash}
chown nombre_de_usuario archivo
~~~

Para cambiar el grupo:

~~~{.bash}
chgrp nombre_de_grupo archivo
~~~

Para cambiar los permisos hay varias formas, la más común:

~~~{.bash}
chmod permisos archivo
~~~

En donde ```permisos``` son tres números, de los cuales el primero se
corresponde a los permisos del usuario, el segundo a los permisos del grupo y
el tercero a los permisos de los demás usuarios. A cada permiso se le da un
número:

- **r** (lectura) = 4
- **w** (escritura) = 2
- **x** (ejecución) = 1

Y para obtener el número que se debe poner al comando se debe sumar ```r+w+x```.
Por ejemplo: 

- Permiso de escritura y lectura es ```r+w = 4+2 = 6```
- Permiso de sólo lectura es ```r = 4```
- Permiso de lectura y ejecución es ```r+x = 4+1 = 5```
- Permiso de lectura, escritura y ejecución ```r+w+x = 4+2+1 = 7```

Como había dicho antes, el comando ```chown``` pide tres números, el primero son
los permisos del usuario, el segundo del grupo y el tercero de los demás
usuarios. 

Entonces por ejemplo si el usuario tiene todos los permisos (```r+w+x = 7```), 
el grupo tiene lectura y ejecución (```r+x = 5```), y los demás usuarios no
tienen permisos (```0```), entonces el comando es:

~~~{.bash}
chmod 750 archivo
~~~

Hay que tener en cuenta que uno tiene que usar _sudo_ si no se es el dueño del
archivo 

### Editar archivos con _nano_

El editor más fácil de usar es _nano_, para editar un archivo:

~~~{.bash}
nano archivo
~~~

Una vez que se está editando, abajo aparecen atajos de teclado para distintas
acciones como salir y guardar. Por ejemplo para guardar dice ```^O```, el
símbolo ```^``` significa _Ctrl_, entonces para guardar hay que usar 
_Ctrl-O_.

Para salir se usa _Ctrl-X_.

### Otras cosas

- Normalmente los archivos de configuración están en la carpeta 
  ```/etc/programa```
- Es bueno hacer una copia de cada archivo de configuración que se modifica para
  volver al original si hacemos algo mal
- Normalmente los archivos de configuración tienen muchas líneas que empiezan
  con ```#```, esos son comentarios y son ignorados, sirven solamente para poner
  notas, da lo mismo si están o no.


Conectarse a la red
-------------------

Si en la instalación todo salió bien ya deberías tener internet. Para probar se
puede usar el comando ```ping``` que muestra el tiempo que tarda un mensaje en
ir a una dirección de internet y volver. Por ejemplo:

~~~{.bash}
ping gnulinuxvagos.es
~~~

### Si falta un firmware

Si no hay conexión, lo más probable es que el problema sea porque necesitás
firmware no libre. La forma más fácil es descargar el firmware desde otra PC
a un pendrive y pasarlo al servidor. Si el problema es de configuración a lo
mejor lo arreglamos después al elegir una IP estática.

Las dos marcas más comunes de placas de red son _Realtek_ y _Atheros_, para
saber si necesitás firmware _Realtek_ o _Atheros_ una forma es leer los mensajes
del sistema. Yo uso:

~~~{.bash}
dmesg | grep ath
~~~

Para buscar ```ath``` de _Atheros_ en los mensajes del sistema y ver si dice
algo de que se necesita un firmware de _Atheros_, o si no:

~~~{.bash}
dmesg | grep rtl
~~~

Para _Realtek_.

El firmware de _Atheros_ está en el paquete ```firmware-atheros``` y el de 
_Realtek_ en ```firmware-realtek```. Se pueden descargar de 
[acá](https://packages.debian.org/jessie/all/firmware-atheros/download) y 
[acá](https://packages.debian.org/jessie/all/firmware-realtek/download).

Hay que guardar el ```.deb``` en un USB e instalarlo desde el servidor, más o
menos así:

~~~{.bash}
sudo mount /dev/sdb1 /mnt/usb
sudo dpkg -i "firmware-???_0.43_all.deb"
sudo umount /mnt/usb
~~~

### Elegir una IP local estática

Por defecto las redes usan _DHCP_, que elige una IP cualquiera de las que tiene
disponible, pero el servidor debe tener siempre la misma IP local. Eso se
configura en el archivo ```/etc/network/interfaces```. Si la IP local cambia
deberíamos tener que aprenderla de nuevo, por eso la ponemos fija y listo.

~~~{.bash}
sudo cp /etc/network/interfaces /etc/network/interfaces.OLD
nano /etc/network/interfaces
~~~

Copio el archivo para dejar un respaldo del original, y lo edito así:

~~~
# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
auto eth0
iface eth0 inet static
    address 192.168.0.150
    netmask 255.255.255.0
    network 192.168.0.0
    broadcast 192.168.0.255
    gateway 192.168.0.1
    dns-nameservers 208.67.222.222 208.67.220.220
~~~

Al _Loopback network interface_ ignorenlo, el importante es el ```eth0``` que
es la primera interfaz de red. Si hay otro puerto más se puede configurar
```eth1```.

Básicamente:

```address```
:    Es la IP local del servidor, depende de la configuración del
     router. Normalmente se usan IPs entre ```192.168.0.1``` y 
     ```192.168.0.254```. No hay que elegir ninguna IP que haga conflicto con
     otro equipo.

```netmask```
:    Es la máscara de red, dice cuáles de los números de la IP identifican la
     red y cuáles identifican al equipo, las redes que empiezan con
     ```192.168``` llevan ```255.255.255.0``` indicando que los primeros tres
     numeros dependen de la red, y el restante depende del equipo.

```network```
:    Es la IP de la red, se obtiene de ```address``` y ```netmask``` dejando
     con ceros los números que identifican al equipo.

```broadcast```
:    Es la IP de broadcast, al enviar algo a esa IP en realidad se lo estaría
     enviando a todos los equipos de la red, es como la ```network``` pero
     con el mayor número de equipo posible.

```gateway```
:    Es la IP del router, a la que van todos los paquetes destinados a un equipo
     fuera de la red (```network```)

```dns-nameservers```
:    IPs de normalmente dos servidores de DNS que traducen nombres
     (```gnulinuxvagos.es```) a IPs (```87.106.185.196```). El dns que elegí
     es _OpenDNS_, pero se puede usar por ejemplo el de _Google_ que sería: 
     ```8.8.8.8``` y ```8.8.4.4```

Hay que tener en cuenta que por ahora la única forma de conectarse al servidor 
es desde dentro de la misma red local y usando la IP local del servidor que
configuramos. Para acceder al servidor desde cualquier parte del mundo hay que
hacer otra cosa que explico más abajo.

Elegir los repositorios
-----------------------

Hay que elegir de donde se van a descargar los paquetes (programas) a usar.
Eso se elige en el archivo ```/etc/apt/sources.list```. Entonces para editarlo:

~~~{.bash}
sudo nano /etc/apt/sources.list
~~~

En ese archivo las líneas que empiezan con ```#``` son comentarios y se ignoran.
Las líneas que uso yo son:

~~~
deb http://ftp.es.debian.org/debian/ stable-updates main contrib non-free
deb-src http://ftp.es.debian.org/debian/ stable-updates main contrib non-free
    
deb http://ftp.es.debian.org/debian/ stable main contrib non-free
deb-src http://ftp.es.debian.org/debian/ stable main contrib non-free
    
deb http://security.debian.org stable/updates main contrib non-free
deb-src http://security.debian.org stable/updates main contrib non-free
~~~

Hay que tener en cuenta que las primeras cuatro dicen ```...ftp.es.debian...```,
depende de donde vivas te puede convenir usar otro país en lugar de ```es```. La
lista está [acá](https://www.debian.org/mirror/list).

Crear los usuarios
------------------

Lo normal es tener un usuario por servicio, todos con contraseña y con los
permisos de los archivos correspondientes bien puestos. Entonces si alguien
logra ingresar desde por ejemplo _GIT_, no puede cambiar nada de los demás
servicios.

Vamos a usar ```nginxvagos```, ```gitvagos``` y ```nfsvagos```. Y además
el usuario con permisos de _sudo_ que maneja todo es ```vago```.

Al usuario ```vago``` ya lo creamos en la instalación, y ya le dimos permisos
de _sudo_. Hay que crear los otros:

~~~{.bash}
sudo adduser nginxvagos
sudo adduser gitvagos
sudo adduser nfsvagos
sudo adduser cupsvagos
~~~

Va a pedir una contraseña para cada uno, obviamente elegir contraseñas
distintas.

SSH
---

Es lo primero a hacer, _SSH_ permite conectarse y manejar el servidor
remotamente, una vez configurado el servidor no necesita teclado ni monitor,
lo manejamos desde otra PC.

Hay que instalar ```openssh-server```:

~~~{.bash}
sudo apt-get install openssh-server
~~~

Se configura todo desde ```/etc/ssh/sshd_config```

~~~{.bash}
sudo nano /etc/ssh/sshd_config
~~~

Las líneas que empiezan con ```#``` son ignoradas, las que son importantes de
tener/cambiar son las siguientes:

~~~
Protocol 2
AllowUsers vago gitvagos
PermitRootLogin no
Port 6543
X11 Forwarding no
MaxAuthTries 2
MaxStartups 5
IgnoreRhosts yes
RhostsRSAAuthentication no
HostbasedAuthentication no
PasswordAuthentication no
PermitEmptyPasswords no
UsePAM yes
AllowTCPForwarding no
~~~

Las que no puse hay que dejarlas como están. Una explicación de qué significa
cada una:

```Protocol 2```
:    Para obligar a usar la versión 2 de SSH, que es más segura.

```AllowUsers vago gitvagos```
:    Especificar qué usuarios pueden loguearse.

```PermitRootLogin no```
:    Para no permitir que alguien entre como _root_ (de todos modos no tenemos
     usuario root)

```Port 6543```
:    Para usar otro puerto en lugar del puerto por defecto (que es el 22). Como
     desventaja hay que especificar puerto cada vez que nos conectamos, algunos
     dicen que es más seguro porque hay scripts automáticos que intentan
     loguearse a puertos 22, pero no creo que aumente mucho la seguridad.
     Se puede usar [cualquier puerto libre](https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers)

```X11 Forwarding no```
:    Es para abrir aplicaciones gráficas del servidor, pero no tenemos interfaz
     gráfica

```MaxAuthTries 2```
:    Máximo de intentos de logueo antes de bloquear a un usuario, el bloqueo es
     muy corto (creo que resetea la conexion) asi que se puede seguir intentando
     de todos modos pero te agrega un retraso.

```MaxStartups 5```
:    Máximo de conexiones simultáneas.

```IgnoreRhosts yes```
:    Es otra forma de loguearse menos segura que no hay que usar, entonces la
     desactivamos.

```RhostsRSAAuthentication no```
:    Este está relacionado con el anterior.

```HostbasedAuthentication no```
:    Este también.

```PasswordAuthentication no```
:    Impedir conectarse con contraseña, obligando a usar un par de claves.

```PermitEmptyPasswords no```
:    Si se pueden usar contraseñas vacías, relacionado con el anterior.

```UsePAM yes```
:    Otra forma de entrar al servidor que no usamos.

```AllowTCPForwarding no```
:    Permite usar al servidor como proxy, no lo usamos.

Para más información está

~~~{.bash}
man sshd_config
~~~

Hay dos formas de conectarse por _SSH_, una es con contraseña y otra es con un
par de claves (más seguro). El cliente (tu PC) debe generar un par de claves, 
una clave privada (que debe quedar secreta en tu PC dentro de un archivo) y una 
pública que se puede compartir a todo el mundo. 
La idea es que un mensaje cifrado con tu clave privada sólo puede ser
descifrado con tu clave pública, y un mensaje cifrado con tu clave pública sólo
puede ser descifrado con tu clave privada.

Lo importante es que cada PC que uses para entrar al servidor va a tener una
clave privada y una pública. A la privada hay que dejarla donde está, y a la
pública hay que guardarla en el servidor para que éste te autorice la conexión.

Se puede proteger la clave privada con contraseña para que si alguien roba la
clave privada no pueda conectarse al servidor. Entonces para conectarse al
servidor se necesita la clave privada y la contraseña.

Ahora hay que crear el par de claves. **Esto se hace en la PC cliente, no se
hace en el servidor**. De paso hay que descargar ```openssh-client``` para
conectarse al servidor.

~~~{.bash}
sudo apt-get install openssh-client
ssh-keygen -t rsa -b 4096 -C "miPC"
~~~

El último parámetro es un comentario para acordarse después de quién era la
clave. No es obligatorio pero es importante poner una contraseña (_passphrase_)
a la clave, o si no lo único que hace falta para entrar es tener la clave
privada. Por defecto la privada se guarda en ```~/.ssh/id_rsa``` y la pública en
```~/.ssh/id_rsa.pub```.

Una vez que se generaron las claves, lo único que hace falta es agregar la clave
**pública** que está en la **PC cliente** en ```~/.ssh/id_rsa.pub``` al archivo
```~/.ssh/authorized_keys``` del **servidor**. En el 
```~/.ssh/authorized_keys``` va una clave pública por renglón, se puede editar
con ```nano``` o usar algo como ```>>``` que agrega al archivo lo que entre por
la izquierda, más abajo está el ejemplo.

Hay que guardar ```~/.ssh/id_rsa.pub``` que está en la PC cliente en un
pendrive, después conectar ese pendrive al servidor y hacer algo como esto:

~~~{.bash}
sudo mount /dev/sdb1 /mnt/usb
mkdir ~/.ssh
cat /mnt/usb/id_rsa.pub >> ~/.ssh/authorized_keys
sudo umount /mnt/usb
sudo /etc/init.d/ssh restart
~~~

Ahora desde la PC cliente se puede probar a conectarse:

~~~{.bash}
ssh -p 6543 vago@192.168.0.150
~~~

Poniendo el puerto, el usuario (del servidor) y la IP correctas. Hay que tener
en cuenta que por ahora hay que estar en la misma red que el servidor.

NGINX
-----

Para hostear una web se puede usar ```nginx``` que es uno de los programas más
usados. Se puede descargar una versión liviana con:

~~~{.bash}
sudo apt-get install nginx-light
~~~

O una versión más completa con:

~~~{.bash}
sudo apt-get install nginx-full
~~~

Yo voy a usar ```nginx-full``` porque uso _Server Side Includes (SSI)_. Que
es una característica que permite incluir un _HTML_ dentro de otro (entre otras
cosas más). Lo uso para reusar el header y el footer de mi página web haciendo
algo así:

~~~
<!--#include virtual="../header.html" -->
~~~

[Acá se puede ver qué tiene ```nginx-full```](https://packages.debian.org/jessie/nginx-full),
y [acá qué hay en ```nginx-light```](https://packages.debian.org/jessie/nginx-light).

Después de instalar se puede probar el sitio configurado por defecto para ver 
que todo esté andando bien.

Básicamente, Para entrar a un sitio web uno debe saber la IP del servidor y
escribirla en la barra de direcciones del navegador que uno use. Lo que se usa
normalmente es un _nombre de dominio_ (por ejemplo www.gnulinuxvagos.es) y lo
que hace el navegador es preguntarle a un servidor de _DNS_ la IP de ese
dominio, pero al final uno necesita saber la IP del servidor.

Por ahora la única forma de conectarse al servidor es desde dentro de la misma
red local y usando la IP local del servidor (la que configuramos más arriba).
Si todo anda bien se debería poder ver un mensaje de bienvenida de _nginx_.

Después se configura en ```/etc/nginx/nginx.conf```, como siempre, las líneas 
que empiezan con ```#``` se ignoran. Entonces para editar:

~~~{.bash}
sudo cp /etc/nginx/nginx.conf /etc/nginx/nginx.conf.OLD
sudo nano /etc/nginx/nginx.conf
~~~

Yo cambié el usuario de ```www-data``` a ```nginxvagos``` porque me gustaba
más, y con ```server_tokens off``` pido que al mostrar un error como 404 no
diga que estoy corriendo ```nginx``` al usuario. Hay que tener en cuenta que
_nginx_ pide poner punto y coma después de cada orden. Queda algo así:

~~~
user nginxvagos;
worker_processes 4;
...

...
http {
    server_tokens off;
    sendfile on;
    ...
~~~

Puse puntos suspensivos porque ahí al medio hay más cosas.

Con eso estaría lista la configuración general. Después hay que configurar cada
sitio que vamos a agregar (se pueden hostear varios sitios pero vamos a hostear
uno solo). Los sitios se configuran en 
```/etc/nginx/sites-available/vagosweb``` por ejemplo. Después para activar ese
sitio hay que hacer un enlace de ese archivo a 
```/etc/nginx/sites-enabled/vagosweb```, un enlace es lo que sería un "Acceso
directo" en _Windows_.

Por defecto viene el sitio llamado ```default```, lo más fácil es partir de ese
archivo para hacer el nuestro. Entonces lo primero que hay que hacer es borrar
el enlace que está en ```/etc/nginx/sites-enabled/default``` para
deshabilitarlo:

~~~{.bash}
sudo rm /etc/nginx/sites-enabled/default
~~~

El archivo de configuración sigue estando en
```/etc/nginx/sites-available/default```, entonces le hacemos una copia para
empezar el nuestro:

~~~{.bash}
sudo cp /etc/nginx/sites-available/default /etc/nginx/sites-available/vagosweb
~~~

Ahora lo podemos editar con:

~~~{.bash}
sudo nano /etc/nginx/sites-available/vagosweb
~~~

Normalmente los sitios web se guardan en la carpeta ```/var/www```, entonces yo
lo voy a guardar en ```/var/www/vagosweb```, después yo cambié el nombre de la
página web principal, puse como principal al archivo ```indice.html```. Y
finalmente activé los _Server Side Includes (SSI)_ que expliqué antes que son
opcionales (si no los usás no los actives), eso se activa con ```ssi on;```.
Queda algo así:

~~~
server {
	listen 80 default_server;
	...

    ...
	root /var/www/vagosweb;
    ssi on;
	index index.shtml;

	server_name _;
    ...
~~~

Ahora hay que activar la web que configuramos haciendo un enlace:

~~~{.bash}
sudo ln -s /etc/nginx/sites-available/vagosweb /etc/nginx/sites-enabled/vagosweb
~~~

Falta guardar los archivos de la web, hay que pasarlos a un pendrive a una
carpeta llamada por ejemplo _vagosweb_, conectarlo al servidor y ponerlos en
la carpeta que pusimos en el archivo de configuración. También hay que acomodar
un poco los permisos para que nadie pueda tocar la web, va a ser necesario ser
superusuario para cambiar algo. En este punto creo que ya habría que reiniciar
_nginx_ para que tome los cambios.

~~~{.bash}
sudo mount /dev/sdb1 /mnt/usb
sudo cp -r /mnt/usb/vagosweb /var/www/vagosweb
sudo umount /mnt/usb
sudo chown -R root /var/www/vagosweb
sudo chmod -R 555 /var/www/vagosweb
sudo /etc/init.d/nginx restart
~~~

Y listo, ahora hay que probar de nuevo a ver si todo funciona conectandose desde
otra PC.

GIT
---

_git_ es un software de control de versiones, enfocado sobre todo a proyectos
de programación. Yo lo uso para sincronizar mis documentos porque tiene varias
ventajas (y desventajas también). No es para cualquiera, te tienen que gustar
estas cosas.

**Ventajas**

- Se mantiene una copia de todo en todas las PCs, se pueden romper varias y no
  se pierde nada. También se puede trabajar sin conexión normalmente (es más,
  uno siempre trabaja sin conexión hasta que se ejecutan los comandos para
  sincronizar).

- Se mantiene una copia de todas las versiones de los archivos, entonces uno
  puede volver atrás y recuperar un archivo borrado/modificado/corrupto. _git_
  guarda una copia de todo en el momento en que se sincroniza (no es
  automático).

- Para ahorrar espacio, las copias anteriores de los archivos no están
  completas, son copias parciales que tienen solamente los cambios hechos de
  una versión a otra.

- Es gratis, libre, y muy usado (para programación)

- Si uno es bueno, se puede sincronizar directamente entre PCs para seguir
  sincronizando trabajos si el servidor se rompe.

- Es fácil de configurar en el servidor (pero tedioso para usar todos los días).

**Desventajas**

- La sincronización no es automática, hay que ejecutar unos comandos para
  sincronizar.

- Es bastante difícil de usar, hay que aprenderse 5 o 6 comandos, normalmente
  se usan 5 nada más. No es fácil recuperar archivos cuando uno quiere volver
  atrás algunas versiones, no es que sea inseguro, sino que hay que saber usar
  algunos comandos más y saber un poco como funciona el programa.

- No es muy eficiente para archivos binarios como fotos o documentos de
  _LibreOffice Writer_/_Microsoft Word_, está diseñado para archivos de texto
  porque las versiones guardadas son copias parciales de los cambios entre
  versión y versión. Pero cuando se modifican archivos binarios, como están
  comprimidos, un cambio chico ya cambia casi todo el archivo. Yo lo uso para
  documentos de _LibreOffice_ e imágenes chicas, no hay que usar _git_ para
  cosas grandes.

Se puede hacer un script para ejecutar los comandos para sincronizar con una
orden, no tengo ningún script, a lo mejor después me hago uno. 

**Cómo usar _git_ todos los días**

_git_ maneja una carpeta llamada repositorio que está en tu PC, adentro están
las cosas a sincronizar. Para ejecutar un comando uno debe hacer ```cd``` y
moverse a la carpeta primero. Habría que buscar un tutorial de _git_, pero
básicamente:

```git status```
:    Para ver los cambios realizados desde la última vez que se hizo 
     ```git commit```

```git add -A```
:    Marcar para incluir a todos los archivos modificados en el próximo
     ```git commit```

```git commit```
:    Crea una copia de todos los archivos incluidos con ```git add```
     y guarda la copia para siempre.

```git pull```
:    Descarga del servidor todos los _commits_ nuevos y por lo tanto todos los
     cambios nuevos.

```git push```
:    Sube al servidor todos los _commits_ nuevos y por lo tanto todos los
     cambios nuevos.

Normalmente yo ejecuto todos esos comandos en ese orden. Las cosas que hay que
tener en cuenta es que:

- Puede pasar que uno haya modificado un archivo, y al hacer ```git pull```
  uno descargue una nueva versión del mismo archivo, en ese caso _git_ no sabe
  con qué versión quedarse, en ese caso _git_ deja ambas versiones y uno debe
  resolver el conflicto manualmente y crear un nuevo _commit_.
  Al usar archivos de texto _git_ es más inteligente y resuelve los conflictos
  solo si los cambios están en distintas líneas (pero no si los cambios están
  en la misma línea).
- No se pueden subir cambios al servidor con ```git push``` si hay _commits_ que
  el servidor tiene y nosotros no (el servidor está adelantado). Siempre hay
  que hacer _pull_ antes de _push_.

### Para configurar el servidor

Es muy fácil, solamente hay que guardar una carpeta en el servidor. Porque al
sincronizar vamos a usar _SSH_ que es seguro y ya está configurado.

Para todo esto vamos a usar el usuario ```gitvagos```.

Para crear un repositorio nuevo hay que usar algunos comandos:

~~~{.bash}
sudo apt-get install git
sudo su gitvagos
cd /home/gitvagos
mkdir repositoriovago
cd repositoriovago
git init --bare
~~~

Y listo. Hay que tener en cuenta que cuando sincronicemos vamos a usar el
usuario ```gitvagos```, entonces hay que pasar las claves públicas de las PCs
que pueden usar este repositorio a ```/home/gitvagos/.ssh/authorized_keys```.

Más arriba ya había puesto como se hace, pero el usuario ```gitvagos``` no tiene
permisos de sudo, así que antes hay que loguearse como el usuario ```vago```
para hacer los comandos que necesiten ```sudo```.
Pero ahora también hay que tener cuidado que no se haga lío con los permisos de
los archivos y no crear las carpetas/archivos con el usuario equivocado, la
versión modificada sería:

Primero como usuario ```vago``` y superusuario copiamos las claves a la carpeta
```home``` del usuario ```gitvagos```, después cambiamos el dueño de esas claves
para darle permisos a ```gitvagos```. Al final nos logueamos como ```gitvagos```
y hacemos lo que falta.

~~~{.bash}
su vago
sudo mount /dev/sdb1 /mnt/usb
sudo cp /mnt/usb/id_rsa.pub /home/gitvagos/id_rsa.pub
sudo chown gitvagos /home/gitvagos/id_rsa.pub
sudo umount /mnt/usb

su gitvagos
mkdir /home/gitvagos/.ssh
cat /home/gitvagos/id_rsa.pub >> /home/gitvagos/.ssh/authorized_keys

su vago
sudo /etc/init.d/ssh restart
~~~

Si las PCs que se pueden conectar a ```gitvagos``` son las mismas que se
pueden conectar a ```vagos```, entonces se pueden copiar los
```authorized_keys``` entre usuario y usuario:

~~~{.bash}
su vago
sudo cp /home/vago/.ssh/authorized_keys /home/gitvagos/claves
sudo chown gitvagos /home/gitvagos/claves

su gitvagos
mkdir /home/gitvagos/.ssh
cat /home/gitvagos/claves >> /home/gitvagos/.ssh/authorized_keys

su vago
sudo /etc/init.d/ssh restart
~~~

Todo este lío con los usuarios pasa porque ```gitvagos``` no debería poder ser
superusuario por temas de seguridad. Seguramente hay formas más rápidas de hacer
todo eso pero la idea está.

### Para configurar el ciente

Hay que abrir una terminal y descargar o _clonar_ el repositorio del servidor.

~~~{.bash}
git clone ssh://gitvagos@192.168.0.150:6543/~/repositoriovago
~~~
En ese comando va el usuario, la IP, el puerto y la dirección a la carpeta del
repositorio. Después de eso ya está todo listo, se puede probar a crear un
archivo, hacer _commit_ y después hacer _push_ al servidor.

Por comodidad, ejecutar estos tres comandos si es la primera vez que usás _git_:

~~~{.bash}
git config --global user.name "Mi nombre (Nombre PC)"
git config --global user.email "mimail@mail.com"
git config --global push.default simple
~~~

NFS
---

Es para hacer un servidor de archivos, fotos, videos, etc.

No es lo mejor para documentos porque no se mantienen backups. Se puede
configurar algún programa manualmente para hacerlos (por ejemplo _rsync_).

Esta configuración sirve para un servidor de _NFS_ que solamente se puede acceder
desde dentro de la red local. Se puede si uno quiere acceder desde fuera, pero
es inseguro, hay que buscarse otra cosa, como _FTP_.

Para poder usar _NFS_ en _Windows_ se necesita una versión _Ultimate_ o algo
así, la cosa es que _NFS_ en _Windows_ es _Premium_ y no está disponible 
siempre.

### Configuración del servidor

Para instalar el servidor _NFS_:

~~~{.bash}
sudo aptitude install nfs-kernel-server
~~~

Hay que crear una carpeta en donde van a estar los archivos, darle los
permisos a todos y poner como dueño de la carpeta a ```nfsvagos```

~~~{.bash}
sudo mkdir /home/nfsvagos/archivos.nfs
sudo chmod 777 /home/nfsvagos/archivos.nfs
sudo chown nfsvagos /home/nfsvagos/archivos.nfs
~~~

Después hay que editar el archivo ```/etc/exports```, en donde se ponen las
carpetas a compartir. Entonces hay que hacer ```sudo nano /etc/exports``` y
dentro poner algo así:

~~~
/home/nfsvagos/archivos.nfs 192.168.0.0/255.255.255.0(rw)
~~~

Hay más opciones para poner en el archivo, con esa línea se da permisos de
lectura y escritura a toda la red.

Y por último un comando para comenzar a compartir:

~~~{.bash}
sudo exportfs -ra
~~~

### Configuración del cliente

Todo esto va en la PC que se va a conectar al servidor, primero hay que crear
una carpeta, por ejemplo:

~~~{.bash}
sudo mkdir /media/usuario/nfs
~~~

Y después se monta la carpeta como si se tratara de un pendrive o un CD

~~~{.bash}
sudo mount -t nfs 192.168.0.120:/home/nfsvagos/archivos.nfs /media/usuario/nfs/
~~~

Y listo. Si uno quiere que se monte automaticamente al iniciar la PC, hay que
agregar una línea al archivo ```/etc/fstab```. Entonces hay que hacer
```sudo nano /etc/fstab``` y al final agregar:

~~~
192.168.0.120:/home/nfsvagos/archivos.nfs /media/usuario/nfs nfs auto 0 0
~~~

CUPS
----

Es bastante rápido y fácil.

### Configuración del servidor

Para instalar CUPS es lo mismo de siempre, además agrego un paquete con los
drivers de muchas impresoras, se pueden descargar paquetes más chicos, pero
la cosa es renegar poco así que instalamos todo y listo.

~~~{.bash}
sudo aptitude install cups printer-driver-all
~~~

El archivo de configuración es ```/etc/cups/cupsd.conf```, entonces hacemos una
copia de respaldo y lo editamos

~~~{.bash}
sudo cp /etc/cups/cupsd.conf /etc/cups/cupsd.conf.OLD
sudo nano /etc/cups/cupsd.conf
~~~

Adentro hay que agregar una orden ```Listen``` más, para que podamos configurar
las impresoras desde otros equipos de la red

~~~
Listen 192.168.0.120:631
~~~

Y en los bloques ```<Location />``` , ```<Location /admin>``` y
```<Location /admin/conf>``` hay que agregar una línea:

~~~
Allow all
~~~

Después hay que darle permisos de administrador al usuario ```cupsvagos```

~~~{.bash}
sudo usermod -aG lpadmin cupsvagos
~~~

Y por último reiniciamos _CUPS_ para que funcionen las configuraciones.

~~~{.bash}
sudo service cups restart
~~~

### Configuración de las impresoras y demás

A partir de ahora para configurar las impresoras se puede ingresar desde un
navegador a ```192.168.0.120:631/admin```, es cuestión de agregar impresoras,
ponerles nombre y listo.

Después es cuestión de agregar impresoras desde las PCs de la red yendo a
"Agregar impresora de red" o algo por el estilo, debería aparecer solo, ya sea
en _Windows_ o en _GNU/Linux_.

Por las dudas si hace falta, la dirección que hay que poner en Windows para
usar la impresora en red es:

~~~
http://192.168.0.120:631/printers/nombre_impresora
~~~

Para acceder al servidor desde fuera de la red
----------------------------------------------

Hay dos tipos de IP, la IP local que solamente es válida para dentro de la red 
local, y la IP pública. la IP pública es normalmente una por modem, o sea, una
por casa. Para acceder al servidor desde fuera de casa hay que usar la IP
pública.

No se puede usar la IP pública para entrar al servidor todavía porque cuando
llega la solicitud de conexión al router, el router no sabe a cuál de las
computadoras de tu casa enviarsela, si al servidor, al celular, la tele, etc.
Entonces hay que decirle al router que a todas las solicitudes de páginas
web hay que redireccionarlas a la IP local del servidor.

Eso se hace entrando a la configuración del router escribiendo la IP local del
router en un navegador, es la IP del _gateway_ que configuramos antes, por
ejemplo ```192.168.0.1```. Después de ahí depende de la marca del router.
Hay que buscar alguna parte que diga _NAT_ o _Port Forwarding_ y poner que
redireccione el puerto 80 a la IP local del servidor. El puerto 80 es el que
se usa por defecto para las páginas web.

Si uno quiere hacer _SSH_ desde fuera de la red también hay que redireccionar
el puerto que configuramos para _SSH_, había puesto el ```6543```.

Entonces ahora para conectarse al servidor se puede usar la IP pública del
router. El problema que queda es que la IP pública suele cambiar, la solución
"formal" sería pedir al proveedor de internet que te dé una IP fija, siempre
se cobra por eso. O si no, hay servicios gratuitos que comunican la IP nueva
cada vez que cambie, y de esa forma te dan un dominio gratis que redirecciona
a tu IP local que tengas en ese momento.

No sé cuál es la mejor forma gratis de obtener una IP fija por ahora, a lo mejor
después lo agrego a la guía.

Para tener tu nombre de dominio
-------------------------------

Para poder entrar al servidor con una dirección más fácil de memorizar que una
IP hay que pedir un nombre de dominio, dependiendo del "final" del nombre
(si es ```.com```, ```.es```, ```.org```, etc.) puede que sea gratis o no.

Pero no se bien como es el tema, a lo mejor después lo agrego a la guía.


